
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;


library UNISIM;
use UNISIM.VComponents.all;

--****************************************************************************
--*
--*   Daq Link Source Card  10GbE - FPGA core     
--*
--*   version xxxxxxx  Released on 
--*
--****************************************************************************







entity DaqLSC_10G is
				 
    Port ( sys_reset 	: in  STD_LOGIC;									-- active high reset of all logic but GTX
           sys_clk		: in  STD_LOGIC;									-- Input clock for logic circuitry, not used by SERDES
			  --
           LinkWe			: in  STD_LOGIC;									-- active low Link Write Enable
           LinkCtrl 		: in  STD_LOGIC;									-- active low, the data is transmitted as a Kontrol word
           LinkData 		: in  STD_LOGIC_VECTOR (63 downto 0); 		-- 64 bit wide link data
           srcID 			: in  STD_LOGIC_VECTOR (15 downto 0);		-- Unique identifier of the data source
           LinkDown 		: out  STD_LOGIC;									-- when low, link is not enabled to transfer data
           LinkFull 		: out  STD_LOGIC;									-- when low, indicates that the link can still receive 16 data words max.
																					-- Additionnal data after those 16 will be lost.
			  --
			  sync_loss 	: out STD_LOGIC;		  							-- goes to '1' (rxusrclk) when SERDES is out of synch
			  status_ce 	: in std_logic;									-- not implemented yet
			  status_addr 	: in STD_LOGIC_VECTOR (15 downto 0);		-- Address of internal status registers (see table below)
			  status_port 	: out STD_LOGIC_VECTOR (63 downto 0);		-- Content of addressed status registers
			  --
			  txusrclk_o 	: out STD_LOGIC;  								-- reconstructed tx clock, to be used to clock sending circuitry
			  rxusrclk_o 	: out STD_LOGIC;  								-- reconstructed rx clock, to be used to clock receiving circuitry
			  --
			  --			  --  Register table.
			  -- 
			  --
			  --  ADDRESS(HEX)           NAME                    Description
			  --
			  --	 0002					Data_cnt                  Number of 64 bit words transfered
              --     0003					Event_cnt                 Number of events transfered
			  --     0004					Block_cnt                 Number of data blocks transfered
			  --     0005					Rec_Packet_cnt		      Number of received packets
			  --	 0006					Status_core_1		      Status of internal state machine, only for experts
			  --	 0007					Sent_Packet_cnt	          Number of sent packets
			  --	 0008					Status_core_2		      Status of internal state machine, only for experts
			  --	 0009					Back_P_cnt			      Number of clock cycles with LinkFull active
			  --	 000a					Version				      Design version number
			  --	 000b					Serdes				      Status of internal SERDES, only for experts
			  --     000c					Retrans_Blck_cnt	      Number of data blocks retransmitted			  
			  --  
			  gtx_reset 	: in std_logic;    								-- active high reset of GTX only
			  gtx_refclk_n : in std_logic; 									-- iob for GTX refclk neg
			  gtx_refclk_p : in std_logic; 									-- iob for GTX refclk pos
			  sfp_rxn 		: in std_logic;      							-- sfp iobs rx neg
			  sfp_rxp 		: in std_logic;									-- sfp iobs rx pos
			  sfp_txn 		: out std_logic;								-- sfp iobs tx neg
			  sfp_txp 		: out std_logic									-- sfp iobs tx pos
			 );
end DaqLSC_10G;



architecture Behavioral of DaqLSC_10G is

-- Local signals

signal sys_reset_bar : std_logic;
signal txusrclk, txusrclk2, rxusrclk, rxusrclk2 : std_logic;
signal serdes_core_clk156_out, xgmii_rx_clk_out, clk_156 : std_logic;

signal serdes_in_sync : std_logic;
signal txdata, rxdata : std_logic_vector(63 downto 0);
signal rxcharisk, txcharisk : std_logic_vector(7 downto 0);

--attribute mark_debug : string;

--attribute mark_debug of txdata, rxdata : signal is "true";
--attribute mark_debug of rxcharisk, txcharisk : signal is "true";

signal gtxrefclk, drp_clk   : std_logic;
signal gtx_rxresetdone, gtx_txresetdone : std_logic;
signal serdes_status :std_logic_vector(31 downto 0);






COMPONENT SLINK_opt_XGMII
port (
	reset						: in std_logic;
	-- FED interface
	SYS_CLK					: in std_logic;
	LINKWe					: in std_logic;
	LINKCtrl				: in std_logic;
	LINKData				: in std_logic_vector(63 downto 0);
	src_ID					: in std_logic_vector(15 downto 0);
	inject_err				: in std_logic_vector(17 downto 0);
	read_CE					: in std_logic;
	Addr					: in std_logic_vector(15 downto 0);
	status_data 			: out std_logic_vector(63 downto 0);
	LINKDown				: out std_logic;
	LINK_LFF				: out std_logic;
	
	-- interface SERDES
	clock					: in std_logic;
	serdes_init				: in std_logic;
	SD_Data_o				: out std_logic_vector(63 downto 0);
	SD_Kb_o					: out std_logic_vector(7 downto 0);
	clock_r					: in std_logic;	
	SD_Data_i				: in std_logic_vector(63 downto 0);
	SD_Kb_i					: in std_logic_vector(7 downto 0);
	
	Serdes_status			: in std_logic_vector(31 downto 0) 

	);
END COMPONENT;

COMPONENT ten_gig_eth_pcs_pma_0_example_design
PORT (
  refclk_p                 : in  std_logic;
  refclk_n                 : in  std_logic;
  dclk                     : in  std_logic;
  core_clk156_out          : out std_logic;
  reset                    : in  std_logic;
  sim_speedup_control      : in std_logic := '0';
  xgmii_txd                : in  std_logic_vector(63 downto 0);
  xgmii_txc                : in  std_logic_vector(7 downto 0);
  xgmii_rxd                : out std_logic_vector(63 downto 0);
  xgmii_rxc                : out std_logic_vector(7 downto 0);
  xgmii_rx_clk             : out std_logic;
  txp                      : out std_logic;
  txn                      : out std_logic;
  rxp                      : in  std_logic;
  rxn                      : in  std_logic;
  pma_loopback             : in std_logic;
  pma_reset                : in std_logic;
  global_tx_disable        : in std_logic;
  pcs_loopback             : in std_logic;
  pcs_reset                : in std_logic;
  test_patt_a_b            : in std_logic_vector(57 downto 0);
  data_patt_sel            : in std_logic;
  test_patt_sel            : in std_logic;
  rx_test_patt_en          : in std_logic;
  tx_test_patt_en          : in std_logic;
  prbs31_tx_en             : in std_logic;
  prbs31_rx_en             : in std_logic;
  set_pma_link_status      : in std_logic;
  set_pcs_link_status      : in std_logic;
  clear_pcs_status2        : in std_logic;
  clear_test_patt_err_count: in std_logic;

  pma_link_status          : out std_logic;
  rx_sig_det               : out std_logic;
  pcs_rx_link_status       : out std_logic;
  pcs_rx_locked            : out std_logic;
  pcs_hiber                : out std_logic;
  teng_pcs_rx_link_status  : out std_logic;
  pcs_err_block_count      : out std_logic_vector(7 downto 0);
  pcs_ber_count            : out std_logic_vector(5 downto 0);
  pcs_rx_hiber_lh          : out std_logic;
  pcs_rx_locked_ll         : out std_logic;
  pcs_test_patt_err_count  : out std_logic_vector(15 downto 0);
  core_status              : out std_logic_vector(7 downto 0);
  resetdone                : out std_logic;
  signal_detect            : in  std_logic;
  tx_fault                 : in  std_logic;
  tx_disable               : out std_logic);
END COMPONENT;

begin



Inst_SLINK_opt_XGMII: SLINK_opt_XGMII
PORT MAP(

-- FROM FED logic	
		reset => sys_reset_bar, -- needs an active low reset
		SYS_CLK => sys_clk,
		
		
-- DATA interface from FED

		
		LINKWe        => LinkWe,
		LINKCtrl      => LinkCtrl,
		LINKData      => LinkData,
		src_ID        => srcID,
		inject_err    => (others =>'0'),
		read_CE       => '0',
		Addr          => status_addr,
		status_data   => status_port,
		serdes_status => serdes_status,
		LINKDown      => LinkDown,
		LINK_LFF      => LinkFull,
		
-- SERDES interface		
		
		clock       => serdes_core_clk156_out, --clk_156_service,         		-- clk tx from SERDES
		serdes_init => serdes_status(0), 	-- status that comes back from GTX
		SD_Data_o   => TXDATA, 			-- data sent to serdes (64 bit)
		SD_Kb_o     => TXCHARISK,         -- control K associated to SD_Data_o (8 bits)
		clock_r     => serdes_core_clk156_out,         -- reconstructed clock from SERDES
		SD_Data_i   => RXDATA,          -- return data from SERDES 64 bit
		SD_Kb_i     => RXCHARISK          -- return control K associated to SD_Data_i (8 bits)

);

XGMII_serdes: ten_gig_eth_pcs_pma_0_example_design
  PORT MAP (
    refclk_p                => gtx_refclk_p,  
    refclk_n                => gtx_refclk_n,
    dclk                    => serdes_core_clk156_out,--clk_156_service,
    core_clk156_out         => serdes_core_clk156_out,
    reset                   => gtx_reset,
    sim_speedup_control     => '0',
    xgmii_txd               => TXDATA,
    xgmii_txc               => TXCHARISK,
    xgmii_rxd               => RXDATA,
    xgmii_rxc               => RXCHARISK,
    xgmii_rx_clk            => xgmii_rx_clk_out,
    txp                     => sfp_txp,
    txn                     => sfp_txn,
    rxp                     => sfp_rxp,
    rxn                     => sfp_rxn,
    pma_loopback            => '0',
    pma_reset               => '0',
    global_tx_disable       => '0',
    pcs_loopback            => '0',
    pcs_reset               => '0',
    test_patt_a_b           => (others => '0'),
    data_patt_sel           => '0',
    test_patt_sel           => '0',
    rx_test_patt_en         => '0',
    tx_test_patt_en         => '0',
    prbs31_tx_en            => '0',
    prbs31_rx_en            => '0',
    set_pma_link_status     => '0',
    set_pcs_link_status     => '0',
    clear_pcs_status2       => '0',
    clear_test_patt_err_count => '0',
  
    pma_link_status         => open,
    rx_sig_det              => open,
    pcs_rx_link_status      => open,
    pcs_rx_locked           => open,
    pcs_hiber               => open,
    teng_pcs_rx_link_status => open,
    pcs_err_block_count     => open,
    pcs_ber_count           => open,
    pcs_rx_hiber_lh         => open,
    pcs_rx_locked_ll        => open,
    pcs_test_patt_err_count => open,
    core_status(0)          => serdes_status(0), -- goes to 1 when PCS block lock aquired
    core_status(7 downto 1) => open,
    resetdone               => serdes_status(1),
    signal_detect           => '1',
    tx_fault                => '0',
    tx_disable              => open
  );

txusrclk_o <= serdes_core_clk156_out;
rxusrclk_o <= serdes_core_clk156_out;
sys_reset_bar <= not(sys_reset);







sync_loss <= not(serdes_status(0));
	

end Behavioral;

