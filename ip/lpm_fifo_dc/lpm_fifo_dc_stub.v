// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.3.1 (win64) Build 1056140 Thu Oct 30 17:07:39 MDT 2014
// Date        : Fri Jun 19 14:55:53 2015
// Host        : PCCMDFPGA running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               C:/xlx_wd_vivado_14_3/pcie_7x_0_example/pcie_7x_0_example.srcs/sources_1/ip/lpm_fifo_dc/lpm_fifo_dc_stub.v
// Design      : lpm_fifo_dc
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v12_0,Vivado 2014.3.1" *)
module lpm_fifo_dc(rst, wr_clk, rd_clk, din, wr_en, rd_en, dout, full, empty, wr_data_count)
/* synthesis syn_black_box black_box_pad_pin="rst,wr_clk,rd_clk,din[65:0],wr_en,rd_en,dout[65:0],full,empty,wr_data_count[5:0]" */;
  input rst;
  input wr_clk;
  input rd_clk;
  input [65:0]din;
  input wr_en;
  input rd_en;
  output [65:0]dout;
  output full;
  output empty;
  output [5:0]wr_data_count;
endmodule
