// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.3.1 (win64) Build 1056140 Thu Oct 30 17:07:39 MDT 2014
// Date        : Wed Jul 15 17:09:27 2015
// Host        : PCCMDFPGA running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               C:/xlx_wd_vivado_14_3/pcie_7x_0_example/pcie_7x_0_example.srcs/sources_1/ip/dist_mem_gen_0/dist_mem_gen_0_stub.v
// Design      : dist_mem_gen_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "dist_mem_gen_v8_0,Vivado 2014.3.1" *)
module dist_mem_gen_0(a, d, dpra, clk, we, qdpo_clk, dpo)
/* synthesis syn_black_box black_box_pad_pin="a[10:0],d[63:0],dpra[10:0],clk,we,qdpo_clk,dpo[63:0]" */;
  input [10:0]a;
  input [63:0]d;
  input [10:0]dpra;
  input clk;
  input we;
  input qdpo_clk;
  output [63:0]dpo;
endmodule
