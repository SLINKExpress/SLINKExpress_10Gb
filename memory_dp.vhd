------------------------------------------------------
-- Component Memory Dual port
--
--  Ver 1.00
--
-- Dominique Gigi May 2015
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Memory is
port 	(
		reset	: in std_logic;
		clock	: in std_logic;
		
		addr_w	: in std_logic_vector(10 downto 0);
		data_w	: in std_logic_vector(63 downto 0);
		wen		: in std_logic;
		
		addr_r	: in std_logic_vector(10 downto 0);
		ren		: in std_logic;
		data_r	: out std_logic_vector(63 downto 0)
	);
end Memory;

architecture behavioral of Memory is



--***********************************************************
--**********************  ALTERA DC FIFO  *******************
--***********************************************************
-- component data_memory
	-- PORT
	-- (
		-- clock			: IN STD_LOGIC  := '1';
		-- data			: IN STD_LOGIC_VECTOR (63 DOWNTO 0);
		-- rdaddress	: IN STD_LOGIC_VECTOR (10 DOWNTO 0);
		-- wraddress	: IN STD_LOGIC_VECTOR (10 DOWNTO 0);
		-- wren			: IN STD_LOGIC  := '0';
		-- q				: OUT STD_LOGIC_VECTOR (63 DOWNTO 0)
	-- );
-- end component;

--***********************************************************
--**********************  XILIXN DC FIFO  *******************
--***********************************************************

COMPONENT DATA_Memory
 PORT (
   clka        : IN STD_LOGIC;
   wea         : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
   addra       : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
   dina        : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
   clkb        : IN STD_LOGIC;
   addrb       : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
   doutb       : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
 );
END COMPONENT;
 
--***********************************************************
--**********************  BEGIN  ****************************
--***********************************************************
begin
 

mem_dp:DATA_Memory
  PORT MAP(
    clock       	=> clock, 
    wren        	=> wen, 
    wraddress   	=> addr_w, 
    data        	=> data_w, 
    rdaddress   	=> addr_r, 
    q					=> data_r 
  );
 
 
end behavioral;