--###################################################
-- clock domain translate
--
-- V1.00 : clock IN should be lower that clock out
-- v1.01 : clock IN and clock out can be anything
-- v2.00 : the reset for clock out is resync
-- V3.00 : all resets are resync but the input signal can't come to close 
--
--


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity resync_v4 is

port (
	reset				: in std_logic;
	 
	clocki			: in std_logic;	
	input				: in std_logic;
	clocko			: in std_logic;
	output			: out std_logic
	);
end resync_v4;

architecture behavioral of resync_v4 is
 

signal reg_1st_stage				: std_logic;

signal reg_2nd_stage				: std_logic_vector(2 downto 0);

signal reg_o						: std_logic;
 
--#################################################
--# here start code
--#################################################
begin

process(reset,clocki)
begin
	if reset = '0' then
		reg_1st_stage 	<=	'0';
	elsif rising_edge(clocki) then
		if input = '1' then
			reg_1st_stage	<= not(reg_1st_stage);
		end if;
	end if;
end process;


process(clocko)
begin
	if rising_edge(clocko) then
		reg_o		<= '0';
		if (reg_2nd_stage(2) = '1' and reg_2nd_stage(1) = '0') or ((reg_2nd_stage(2) = '0' and reg_2nd_stage(1) = '1')) then
			reg_o	<= '1';
		end if;
		reg_2nd_stage(2 downto 1) <= reg_2nd_stage(1 downto 0);
		reg_2nd_stage(0)	<= reg_1st_stage;
	end if;
end process;
 
output	<= reg_o;

end behavioral;
